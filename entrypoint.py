import os

from flask_api_rest import create_app

settings_module = os.getenv('APP_SETTINGS_MODULE')
app = create_app(settings_module)